from django import template
from cart.models import *

register = template.Library()


@register.inclusion_tag('core/top-cart-icon.html', takes_context=True)
def top_cart_icon(context):
    cart_id = context['request'].session.get('cart_id')
    if cart_id:
        products = ProductsInCart.objects.filter(
            cart__pk=cart_id
        ).count()
    else:
        products = 0
    return {
        'products': products,
    }
