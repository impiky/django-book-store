from django.urls import path
from .views import *
from products.apiview import ListProducts

app_name = "products_book"

urlpatterns = [
    path('product_create/', CreateBookView.as_view(), name='product-create'),
    path('product_update/<int:pk>/', UpdateBookView.as_view(), name='product-update'),
    path('product_detail/<int:pk>/', DetailBookView.as_view(), name='product-detail'),
    path('product_list/', ListBookView.as_view(), name='product-list'),
    path('product_delete/<int:pk>/', DeleteBookView.as_view(), name='product-delete'),
    path('view-book/<int:pk>/', ListProducts.as_view()),
]