from django.db import models
import datetime
import re
from reference.models import *
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator


# Create your models here.


class Product(models.Model):
    title = models.CharField(verbose_name="Название", max_length=100)
    product_photo = models.ImageField(
        verbose_name="Фото обложки", upload_to='covers/')
    price = models.DecimalField(
        verbose_name="Цена", max_digits=5, decimal_places=2)
    author = models.ManyToManyField(Author, verbose_name="Автор(ы)")
    series = models.ForeignKey(
        Series, verbose_name="Серия", on_delete=models.CASCADE, null=True, blank=True)
    genre = models.ForeignKey(
        Genre, verbose_name="Жанр", on_delete=models.CASCADE)
    year = models.IntegerField(verbose_name="Год издания", max_length=4)
    pages = models.IntegerField(
        verbose_name="Количество страниц", max_length=4)
    cover = models.CharField(verbose_name="Переплет", max_length=10)
    size = models.CharField(verbose_name="Формат", max_length=20)
    isbn_number = models.CharField(verbose_name="ISBN", max_length=13, validators=[RegexValidator(
        regex="^\d{9}[\d|X]$", message="ISBN must be 10 integer digits (allowed X on the last position)", code="invalid_ISBN")])
    weight = models.IntegerField(verbose_name="Вес,гр.", max_length=4)
    age_restriction = models.IntegerField(
        verbose_name="Возрастные ограничения", max_length=3)
    publisher = models.ForeignKey(
        Publisher, verbose_name="Издательство", on_delete=models.CASCADE)
    manufacturer = models.ForeignKey(Manufacturer,
                                     verbose_name="Изготовитель", on_delete=models.CASCADE)
    in_stock_value = models.IntegerField(
        verbose_name="В наличии", max_length=4)
    order_possibility = models.BooleanField(
        verbose_name="Доступность для заказа", default=True)
    rating = models.IntegerField(
        verbose_name="Рейтинг", max_length=2, default=0)
    addition_date = models.DateField(verbose_name="Дата внесения в каталог")
    last_change = models.DateField(
        verbose_name="Дата последнего изменения", default=datetime.date.today)
    promotion = models.BooleanField(
        verbose_name="Товар на акции", default=False)
    voted = models.IntegerField(verbose_name="Проголосовавших", default=0)
    category = models.ForeignKey(
        Category, verbose_name="Категория", null=True, default="Книги", on_delete=models.CASCADE)

    def __str__(self):
        return self.title
