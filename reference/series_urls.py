from django.urls import path
from .views import *

app_name = "reference_series"

urlpatterns = [
    path('series_ref_create/', SeriesRefCreateView.as_view(), name='series-create'),
    path('series_ref_update/<int:pk>/', SeriesRefUpdateView.as_view(), name='series-update'),
    path('series_ref_detail/<int:pk>/', DetailSeriesView.as_view(), name='series-detail'),
    path('series_ref_list/', ListSeriesView.as_view(), name='series-list'),
    path('series_ref_delete/<int:pk>/', DeleteSeriesView.as_view(), name='series-delete'),
]