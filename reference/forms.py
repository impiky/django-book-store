from django import forms
from .models import *

class AuthorRefCreateForm(forms.ModelForm):
    # name = forms.CharField(widget=forms.TextInput(attrs={'placeholder' : 'It\'s name'}))
    # description = forms.CharField()
    class Meta:
        model=Author
        fields=[
            'author'
        ]


class SeriesRefCreateForm(forms.ModelForm):
    class Meta:
        model=Series
        fields=[
            'genre',
            'description'
        ]

class PublisherRefCreateForm(forms.ModelForm):
    class Meta:
        model=Publisher
        fields=[
            'publisher'
        ]

class GenreRefCreateForm(forms.ModelForm):
    class Meta:
        model=Genre
        fields=[
            'genre'
        ]

class ManufacturerRefCreateForm(forms.ModelForm):
    class Meta:
        model=Manufacturer
        fields=[
            'manufacturer'
        ]

class CategoryRefCreateForm(forms.ModelForm):
    class Meta:
        model=Category
        fields=[
            'category'
        ]
   