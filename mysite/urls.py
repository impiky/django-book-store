"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from reference.views import *
from products.views import *
from core.views import *
from profiles_app.views import UserHomeProfileAddressView
from admincoree.views import *
from django.conf import settings
from django.conf.urls.static import static
from search.views import *
from django.contrib.auth.views import (
    LoginView,
    LogoutView,
    PasswordResetView,
    PasswordResetDoneView,
    PasswordResetConfirmView,
    PasswordResetCompleteView,
)
import debug_toolbar
from rest_framework import routers
from reference.apiview import AuthorViewSet, SerieViewSet


router = routers.DefaultRouter()
router.register(r'api/author', AuthorViewSet)
router.register(r'api/serie', SerieViewSet)


urlpatterns = [
    path('admin/', admin.site.urls),
    path('admin-shop/products/',
         include('products.product_urls', namespace='products_book')),
    path('admin-shop/ref/', include('reference.author_urls',
                                    namespace='reference_author')),
    path('admin-shop/ref/', include('reference.series_urls',
                                    namespace='reference_series')),
    path('admin-shop/ref/', include('reference.publisher_urls',
                                    namespace='reference_publisher')),
    path('admin-shop/ref/', include('reference.genre_urls',
                                    namespace='reference_genre')),
    path('admin-shop/ref/', include('reference.manufacturer_urls',
                                    namespace='reference_manufacturer')),
    path('cart/', include('cart.urls', namespace='cart')),
    path('about/', UserHomeAboutTemplateView.as_view(), name='about-store'),
    path('delivery/', UserHomeDeliveryTemplateView.as_view(), name='delivery-store'),
    path('profile/', UserHomeProfileTemplateView.as_view(), name='profile'),
    path('profile/address/<str:object>/', UserHomeProfileAddressView.as_view(), name='profile-address'),
    path('admin-shop/', StaffHomeTemplateView.as_view(), name='staff-page'),
    path('admin-shop/statistics/', Statistics.as_view(), name='staff-stats'),
    path('orders/', include('orders.urls', namespace="orders")),
    path('cart/', include('cart.urls', namespace="cart")),
    path('search/', SearchView.as_view()),
    path('makesearch/', MakeSearchView.as_view(), name = "makesearch"),
    path('accounts/login/', LoginView.as_view(template_name='registration/login.html',
                                              redirect_authenticated_user=True, redirect_field_name=" "), name='login'),
    path('accounts/logout/',
         LogoutView.as_view(template_name='registration/logged_out.html')),
    path('change-password/', ChangePasswordView.as_view(), name="change-password"),
    path('change-password/done/', ChangePasswordDoneView.as_view(),
         name="password_change_done"),
    path('password-reset/', PasswordResetView.as_view(
        template_name='password/reset-password.html'), name="password-reset"),
    path('password-reset/done/', PasswordResetDoneView.as_view(
        template_name='password/reset-password-done.html'), name="password_reset_done"),
    path('password-reset/<uidb64>/<token>/', PasswordResetConfirmView.as_view(template_name='password/password-reset-confirm.html'), name="password_reset_confirm"),
    path('reset/done/', PasswordResetCompleteView.as_view(template_name='password/password-reset-complete.html'), name='password_reset_complete'),
    path('', UserHomeTemplateView.as_view()),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),

    ] + urlpatterns

urlpatterns += router.urls
