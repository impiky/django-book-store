from django import forms
from django.contrib.auth.forms import PasswordChangeForm, PasswordResetForm
from django.contrib.auth.models import User

class ChangePasswordForm(PasswordChangeForm):
    template_name='password/change-password.html'
    old_password = forms.CharField(
        label=("Старый пароль"),
        strip=False,
        widget=forms.PasswordInput(attrs={'autofocus': True, 'placeholder': 'Старый пароль',
                               'class': 'form-control', 'required': 'true'}),
    )

    new_password1 = forms.CharField(
        label=("Новый пароль"),
        strip=False,
        widget=forms.PasswordInput(attrs={'placeholder': 'Новый пароль',
                               'class': 'form-control', 'required': 'true'}),
    )

    new_password2 = forms.CharField(
        label=("Повторите пароль"),
        strip=False,
        widget=forms.PasswordInput(attrs={'placeholder': 'Повторите пароль',
                               'class': 'form-control', 'required': 'true'}),
    )

class UserProfileForm(forms.ModelForm):
    class Meta:
        model=User
        fields=[
            'first_name',
            'last_name',
            'email',
        ]

    