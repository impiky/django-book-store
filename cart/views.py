from django.shortcuts import render
from django.views.generic import *
from .forms import AddToCartForm
from .models import * 
from products.models import *
from orders.forms import CheckoutOrderForm
from reference.models import OrderStatus

new_order_status = OrderStatus.objects.get(pk=1)

class AddProductToCartView(UpdateView):
    template_name = "cart/add-to-cart.html"
    form_class = AddToCartForm
    model = ProductsInCart

    def get_object(self):
        cart_id = self.request.session.get('cart_id')
        book_id = self.kwargs.get('product')  
        usr = None
        if self.request.user.is_authenticated:
            usr = self.request.user
        cart, cart_created = Cart.objects.get_or_create(
            pk = cart_id,
            defaults = {
                'user': usr,
            }
        )
        if cart_created:
            self.request.session['cart_id'] = cart.pk
        book_id = self.kwargs.get('product')  
        book = Product.objects.get(pk=book_id)
        obj, created = self.model.objects.get_or_create(
            cart = cart,
            book = book,
            defaults = {
                'cart': cart,
                'book': book,
                'quantity': 0
            }
        ) 
        return obj
    
    def get_success_url(self):
        return self.request.POST.get('next', '/')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["next"] = self.request.GET.get('next', '/')
        return context

class ListCartView(DetailView):
    template_name = "cart/list-cart.html"
    model = Cart
    
    def get_object(self):
        cart_id = self.request.session.get('cart_id')
        cart, cart_created = Cart.objects.get_or_create(
            pk = cart_id,
            defaults = {
                'user': self.request.user,
            }
        )
        return cart 

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        checkout_form = CheckoutOrderForm()
        checkout_form.fields['cart'].initial = self.object
        checkout_form.fields['status'].initial = new_order_status
        context["form"] = checkout_form
        return context
    
class DeleteItemDeleteView(DeleteView):
    model = ProductsInCart
    template_name = 'cart/delete-item.html'

    def get_success_url(self):
        return self.request.POST.get('next', '/')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["next"] = self.request.GET.get('next', '/')
        return context