from django.shortcuts import render
from comments.models import Comment
from django.views.generic import RedirectView
from django.contrib.contenttypes.models import ContentType, get_for_id

class SaveComment(RedirectView):
    model = Comment

    def get_redirect_url(self):
        comment = self.request.POST.get(body)
        content_type = self.request.POST.get(content_type)
        object_id = self.request.POST.get(object_id)
        ct = ContentType.get_for_id(object_id)
        comment = Comment.objects.get_or_create(
            comment = comment,
            ct = ct,
            object_id = object_id,
        )
        return comment
