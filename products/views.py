from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic import DetailView, ListView, DeleteView
from .forms import BookForm
from .models import Product

# Create your views here.


class CreateBookView(CreateView):
    form_class = BookForm
    template_name = 'book-create.html'
    success_url = '/admin-shop/products/product_list/'

    def get_context_data(self, *args, **kwargs):
        context = super(CreateBookView, self).get_context_data(*args, **kwargs)
        context['ref_action'] = 'создания нового товара'
       # context['temp'] = self.kwargs # получение id в виде словаря {'pk' : int:id}
        return context


class UpdateBookView(UpdateView):
    template_name = 'book-create.html'
    success_url = '/admin-shop/products/product_list/'
    model = Product
    fields = ['title',
              'product_photo',
              'price',
              'author',
              'series',
              'genre',
              'year',
              'pages',
              'cover',
              'size',
              'isbn_number',
              'weight',
              'age_restriction',
              'publisher',
              'manufacturer',
              'in_stock_value',
              'order_possibility',
              'addition_date',
              'last_change',
              'promotion'
              ]

    def get_context_data(self, *args, **kwargs):
        context = super(UpdateBookView,
                        self).get_context_data(*args, **kwargs)
        context['ref_action'] = 'редактирования товара'
        return context


class DetailBookView(DetailView):
    template_name = 'detail-base-product.html'
    model = Product


class ListBookView(ListView):
    template_name = 'list-book-ref.html'
    model = Product
    paginate_by = 3

    def get_context_data(self, *args, **kwargs):
        context = super(ListBookView, self).get_context_data(*args, **kwargs)
        context['descr'] = 'товаров'
        return context


class DeleteBookView(DeleteView):
    template_name = 'delete-base-product.html'
    success_url = '/admin-shop/products/product_list/'
    model = Product
    fields = ['title',
              'product_photo',
              'price',
              'author',
              'series',
              'genre',
              'year',
              'pages',
              'cover',
              'size',
              'isbn_number',
              'weight',
              'age_restriction',
              'publisher',
              'manufacturer',
              'in_stock_value',
              'order_possibility',
              'addition_date',
              'last_change',
              'promotion'
              ]

    def get_context_data(self, *args, **kwargs):
        context = super(DeleteBookView, self).get_context_data(*args, **kwargs)
        context['ref_action'] = self.object
        return context
