from django.urls import path
from .views import *

app_name = "cart"

urlpatterns = [
    path('add/<int:product>/', AddProductToCartView.as_view(), name='add'),
    path('list/', ListCartView.as_view(), name='list'),
    path('delete/<int:pk>', DeleteItemDeleteView.as_view(), name="delete"),
]