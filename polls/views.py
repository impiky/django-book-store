from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import TemplateView
import calendar,datetime



# Create your views here.

def index(request):
    return HttpResponse('Hello, world!')

def dynamic_view(request, name):
    response = HttpResponse("""<h1>Hello, {}!<h1>
    </p> today is  {}""".format(name,calendar.day_name[datetime.datetime.today().weekday()]))
    return response
