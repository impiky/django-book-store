from django.urls import path
from .views import *

app_name = "reference_publisher"

urlpatterns = [
    path('publisher_ref_create/', PublisherRefCreateView.as_view(), name='publisher-create'),
    path('publisher_ref_update/<int:pk>/', PublisherRefUpdateView.as_view(), name='publisher-update'),
    path('publisher_ref_detail/<int:pk>/', DetailPublisherView.as_view(), name='publisher-detail'),
    path('publisher_ref_list/', ListPublisherView.as_view(), name='publisher-list'),
    path('publisher_ref_delete/<int:pk>/', DeletePublisherView.as_view(), name='publisher-delete'),
]