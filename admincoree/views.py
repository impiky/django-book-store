from django.views.generic import TemplateView
from products.models import Product
from reference.models import Category
import datetime,calendar


# Create your views here.
class StaffHomeTemplateView(TemplateView):
    template_name = 'staff-home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['logo'] = 'BookStore admin'
        context['active'] = 'staff-page'
        return context

class Statistics(TemplateView):
    template_name = 'stats.html'
    def get_context_data(self, **kwargs):
        date = datetime.date.today()
        context = super().get_context_data(**kwargs)
        context['logo'] = 'BookStore admin'
        context['active'] = 'staff-page'
        context['all_products'] = Product.objects.count()
        context['active_products'] = Product.objects.filter(order_possibility__exact=True).count()
        context['added_yesterday'] = Product.objects.filter(addition_date__exact=datetime.datetime.now() - datetime.timedelta(days = 1)).count()
        context['added_per_month'] = Product.objects.filter(addition_date__month=date.month).count()
        context['category'] = Category.objects.count()
        context['category_books'] = Product.objects.filter(category__category__icontains="Книги").count()
        context['category_magazines'] = Product.objects.filter(category__category__icontains="Журналы").count()
        context['category1_name'] = Category.objects.get(id=2)
        context['category2_name'] = Category.objects.get(id=1)
        return context
    

