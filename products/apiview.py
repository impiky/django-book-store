from rest_framework.views import APIView
from rest_framework.generics import RetrieveAPIView
from rest_framework import authentication, permissions
from .models import Product
from .serializers import ProductSerializer

class ListProducts(RetrieveAPIView):
    
    queryset = Product.objects.all()
    model = Product
    serializer_class = ProductSerializer