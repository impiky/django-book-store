from rest_framework import serializers
from .models import Author, Series


class AuthorSerializer(serializers.ModelSerializer):
    total = serializers.SerializerMethodField()
    class Meta:
        model = Author
        fields = (
            'pk',
            'author', 
            'total',
            )
    
    def get_total(self, obj):
        return obj.author + ' ' + 'value'

class SerieSerializer(serializers.ModelSerializer):
    class Meta:
        model = Series
        fields = (
            'pk',
            'genre', 
            'description',
            )