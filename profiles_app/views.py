from django.shortcuts import render
from .models import ProfileAppModel
from django.views.generic.edit import UpdateView
from django.shortcuts import get_object_or_404
import requests 


class UserHomeProfileAddressView(UpdateView):
    template_name = 'address.html'
    fields = [
                'customer',
                'delivery_address',
    ]
    model = ProfileAppModel
    success_url = '/profile/'
    
    def get_object(self):
        customer_id = self.request.user.pk
        if customer_id == 1:
            obj, created =  self.model.objects.only('id').get_or_create(pk=customer_id+1, defaults = {
                    'pk': customer_id,
                })
        else: 
            obj, created =  self.model.objects.only('id').get_or_create(pk=customer_id-1, defaults = {
                    'pk': customer_id,
                })
        print(customer_id)
        return obj