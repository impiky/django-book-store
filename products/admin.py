from django.contrib import admin
from .models import Product, Author

# Register your models here.
admin.site.register(Product)
