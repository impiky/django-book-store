from django.urls import path

from .views import *

app_name = "orders"

urlpatterns = [
    path(
        'check-out',
        OrderCheckoutView.as_view(),
        name="check-out"),
    path(
        'success/<int:pk>/',
        OrderCheckoutSuccess.as_view(),
        name="success"),
    # path(
    #     'list/',
    #     views.OrdersList.as_view(),
    #     name="list")
    path(
        'list-admin/',
        OrdersListAdmin.as_view(),
        name="list-admin"),
    path('delete/<int:pk>', OrderDelete.as_view(), name="delete"),
    path('detail/<int:pk>', OrderDetailView.as_view(), name="detail"),
    path('update/<int:pk>', OrderUpdateView.as_view(), name="update")
]
