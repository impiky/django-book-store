from django.urls import path
from .views import *
from reference.apiview import ListAuthor


app_name = "reference_author"

urlpatterns = [
    path('author_ref_create/', AuthorRefCreateView.as_view(), name='author-create'),
    path('author_ref_update/<int:pk>/', AuthorRefUpdateView.as_view(), name='author-update'),
    path('author_ref_detail/<int:pk>/', DetailAuthorView.as_view(), name='author-detail'),
    path('author_ref_list/', ListAuthorView.as_view(), name='author-list'),
    path('author_ref_delete/<int:pk>/', DeleteAuthorView.as_view(), name='author-delete'),
    path('view-author/<int:pk>/', ListAuthor.as_view()),
]