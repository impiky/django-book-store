from django.urls import path
from .views import *

app_name = "reference_genre"

urlpatterns = [
    path('genre_ref_create/', GenreRefCreateView.as_view(), name='genre-create'),
    path('genre_ref_update/<int:pk>/', GenreRefUpdateView.as_view(), name='genre-update'),
    path('genre_ref_detail/<int:pk>/', DetailGenreView.as_view(), name='genre-detail'),
    path('genre_ref_list/', ListGenreView.as_view(), name='genre-list'),
    path('genre_ref_delete/<int:pk>/', DeleteGenreView.as_view(), name='genre-delete'),
]