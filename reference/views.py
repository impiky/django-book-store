from django.shortcuts import render
from .forms import *
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic import DetailView, ListView, DeleteView
from django.urls import reverse
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin


# Create your views here.

# def author_ref_create(request):
#     context={}
#     print(request.method)
#     if request.method == 'POST':
#         form = AuthorRefCreateForm(request.POST)
#         context['test'] = "Im from POST"
#         if form.is_valid():
#             form.save()
#     else:
#         form = AuthorRefCreateForm()
#         context['test'] = "Im from GET"
#     context['form'] = form
#     return render(request, 'author_ref_create.html', context=context)


class AuthorRefCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    form_class = AuthorRefCreateForm
    template_name = 'create-base-ref.html'
    permission_required = 'reference.add_author' 
    success_url = '/admin-shop/ref/author_ref_list/'
    permission_required = 'reference.can_add'
    def get_context_data(self, *args ,**kwargs):
        context = super(AuthorRefCreateView, self).get_context_data(*args, **kwargs)
        context['ref_action'] = 'создания нового автора'
       # context['temp'] = self.kwargs # получение id в виде словаря {'pk' : int:id}
        return context

class AuthorRefUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    template_name = 'create-base-ref.html'
    permission_required = 'reference.change_author'
    success_url = '/admin-shop/ref/author_ref_list/'
    permission_required = 'reference.can_update'
    model = Author
    fields = ['author']
    def get_context_data(self, *args ,**kwargs):
        context = super(AuthorRefUpdateView, self).get_context_data(*args, **kwargs)
        context['ref_action'] = 'редактирования автора'
        return context

class DetailAuthorView(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    template_name = 'detail-base-ref.html'
    permission_required = 'reference.view_author' 
    model = Author

class ListAuthorView(LoginRequiredMixin, ListView):
    template_name = 'list-author-ref.html'
    model = Author
    paginate_by = 3
    def get_context_data(self, *args ,**kwargs):
        context = super(ListAuthorView, self).get_context_data(*args, **kwargs)
        context['descr'] = 'авторов'
        return context

class DeleteAuthorView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    template_name = 'delete-base-ref.html'
    permission_required = 'reference.delete_author' 
    success_url = '/admin-shop/ref/author_ref_list/'
    permission_required = 'reference.can_delete'
    model = Author
    fields = ['author']
    def get_context_data(self, *args ,**kwargs):
        context = super(DeleteAuthorView, self).get_context_data(*args, **kwargs)
        context['ref_action'] = self.object
        return context

class SeriesRefCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    form_class = SeriesRefCreateForm
    template_name = 'create-base-ref.html'
    permission_required = 'reference.add_series'
    success_url = '/admin-shop/ref/series_ref_list/'
    permission_required = 'reference.can_add'
    def get_context_data(self, *args ,**kwargs):
        context = super(SeriesRefCreateView, self).get_context_data(*args, **kwargs)
        context['ref_action'] = 'добавления серии'
        return context

class SeriesRefUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    template_name = 'create-base-ref.html'
    permission_required = 'reference.change_series'
    success_url = '/admin-shop/ref/series_ref_list/'
    permission_required = 'reference.can_update'
    model = Series
    fields = ['genre',
              'description'
    ]
    def get_context_data(self, *args ,**kwargs):
        context = super(SeriesRefUpdateView, self).get_context_data(*args, **kwargs)
        context['ref_action'] = 'редактирования серии'
        return context

class DetailSeriesView(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    template_name = 'detail-base-ref.html'
    permission_required = 'reference.can_view'
    model = Series

class ListSeriesView(LoginRequiredMixin, ListView):
    template_name = 'list-series-ref.html'
    model = Series
    paginate_by = 3
    def get_context_data(self, *args ,**kwargs):
        context = super(ListSeriesView, self).get_context_data(*args, **kwargs)
        context['descr'] = 'серий'
        return context

class DeleteSeriesView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    template_name = 'delete-base-ref.html'
    permission_required = 'reference.delete_series'
    success_url = '/admin-shop/ref/publisher_ref_list/'
    permission_required = 'reference.can_delete'
    model = Series
    fields = ['genre',
              'description'
    ]
    def get_context_data(self, *args ,**kwargs):
        context = super(DeleteSeriesView, self).get_context_data(*args, **kwargs)
        context['ref_action'] = self.object
        return context
    
class PublisherRefCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    form_class = PublisherRefCreateForm
    template_name = 'create-base-ref.html'
    permission_required = 'reference.add_publisher'
    success_url = '/admin-shop/ref/publisher_ref_list/'
    permission_required = 'reference.can_add'
    def get_context_data(self, *args ,**kwargs):
        context = super(PublisherRefCreateView, self).get_context_data(*args, **kwargs)
        context['ref_action'] = 'добавления издателя'
        return context

class PublisherRefUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    template_name = 'create-base-ref.html'
    permission_required = 'reference.change_publisher'
    success_url = '/admin-shop/ref/publisher_ref_list/'
    permission_required = 'reference.can_update'
    model = Publisher
    fields = ['publisher']
    def get_context_data(self, *args ,**kwargs):
        context = super(PublisherRefUpdateView, self).get_context_data(*args, **kwargs)
        context['ref_action'] = 'редактирования издателя'
        return context

class DetailPublisherView(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    template_name = 'detail-base-ref.html'
    permission_required = 'reference.can_view'
    model = Publisher

class ListPublisherView(LoginRequiredMixin, ListView):
    template_name = 'list-publisher-ref.html'
    model = Publisher
    paginate_by = 3
    def get_context_data(self, *args ,**kwargs):
        context = super(ListPublisherView, self).get_context_data(*args, **kwargs)
        context['descr'] = 'издателей'
        return context

class DeletePublisherView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    template_name = 'delete-base-ref.html'
    success_url = '/admin-shop/ref/publisher_ref_list/'
    permission_required = 'reference.can_delete'
    model = Publisher
    fields = ['publisher']
    def get_context_data(self, *args ,**kwargs):
        context = super(DeletePublisherView, self).get_context_data(*args, **kwargs)
        context['ref_action'] = self.object
        return context

class GenreRefCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    form_class = GenreRefCreateForm
    template_name = 'create-base-ref.html'
    permission_required = 'reference.add_genre'
    success_url = '/admin-shop/ref/genre_ref_list/'
    permission_required = 'reference.can_add'
    def get_context_data(self, *args ,**kwargs):
        context = super(GenreRefCreateView, self).get_context_data(*args, **kwargs)
        context['ref_action'] = 'добавления жанра'
        return context

class GenreRefUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    template_name = 'create-base-ref.html'
    permission_required = 'reference.change_genre'
    success_url = '/admin-shop/ref/genre_ref_list/'
    permission_required = 'reference.can_update'
    model = Genre
    fields = ['genre']
    def get_context_data(self, *args ,**kwargs):
        context = super(GenreRefUpdateView, self).get_context_data(*args, **kwargs)
        context['ref_action'] = 'редактирования жанра'
        return context

class DetailGenreView(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    template_name = 'detail-base-ref.html'
    permission_required = 'reference.can_view'
    model = Genre

class ListGenreView(LoginRequiredMixin, ListView):
    template_name = 'list-genre-ref.html'
    model = Genre
    paginate_by = 3
    def get_context_data(self, *args ,**kwargs):
        context = super(ListGenreView, self).get_context_data(*args, **kwargs)
        context['descr'] = 'жанров'
        return context

class DeleteGenreView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    template_name = 'delete-base-ref.html'
    permission_required = 'reference.delete_genre'
    success_url = '/admin-shop/ref/genre_ref_list/'
    permission_required = 'reference.can_delete'
    model = Genre
    fields = ['genre']
    def get_context_data(self, *args ,**kwargs):
        context = super(DeleteGenreView, self).get_context_data(*args, **kwargs)
        context['ref_action'] = self.object
        return context

class ManufacturerRefCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    form_class = ManufacturerRefCreateForm
    template_name = 'create-base-ref.html'
    permission_required = 'reference.add_manufacturer'
    success_url = '/admin-shop/ref/manufacturer_ref_list/'
    permission_required = 'reference.can_add'
    def get_context_data(self, *args ,**kwargs):
        context = super(ManufacturerRefCreateView, self).get_context_data(*args, **kwargs)
        context['ref_action'] = 'добавления производителя'
        return context

class ManufacturerRefUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    template_name = 'create-base-ref.html'
    permission_required = 'reference.change_manufacturer'
    success_url = '/admin-shop/ref/manufacturer_ref_list/'
    permission_required = 'reference.can_update'
    model = Manufacturer
    fields = ['manufacturer']
    def get_context_data(self, *args ,**kwargs):
        context = super(ManufacturerRefUpdateView, self).get_context_data(*args, **kwargs)
        context['ref_action'] = 'редактирования производителя'
        return context

class DetailManufacturerView(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    template_name = 'detail-base-ref.html'
    permission_required = 'reference.can_view'
    model = Manufacturer

class ListManufacturerView(LoginRequiredMixin, ListView):
    template_name = 'list-manufacturer-ref.html'
    model = Manufacturer
    paginate_by = 3
    def get_context_data(self, *args ,**kwargs):
        context = super(ListManufacturerView, self).get_context_data(*args, **kwargs)
        context['descr'] = 'производителей'
        return context

class DeleteManufacturerView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    template_name = 'delete-base-ref.html'
    permission_required = 'reference.delete_manufacturer'
    success_url = '/admin-shop/ref/manufacturer_ref_list/'
    permission_required = 'reference.can_delete'
    model = Manufacturer
    fields = ['manufacturer']
    def get_context_data(self, *args ,**kwargs):
        context = super(DeleteManufacturerView, self).get_context_data(*args, **kwargs)
        context['ref_action'] = self.object
        return context







# Удаление методом POST