from django.urls import path
from .views import *

app_name = "reference_manufacturer"

urlpatterns = [
    path('manufacturer_ref_create/', ManufacturerRefCreateView.as_view(), name='manufacturer-create'),
    path('manufacturer_ref_update/<int:pk>/', ManufacturerRefUpdateView.as_view(), name='manufacturer-update'),
    path('manufacturer_ref_detail/<int:pk>/', DetailManufacturerView.as_view(), name='manufacturer-detail'),
    path('manufacturer_ref_list/', ListManufacturerView.as_view(), name='manufacturer-list'),
    path('manufacturer_ref_delete/<int:pk>/', DeleteManufacturerView.as_view(), name='manufacturer-delete'),
]