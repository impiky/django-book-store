from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Series)
admin.site.register(Publisher)
admin.site.register(Author)
admin.site.register(Genre)
admin.site.register(Manufacturer)
admin.site.register(Category)
admin.site.register(OrderStatus)
