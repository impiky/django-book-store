from django.views.generic import TemplateView
from products.models import Product
from django.db.models import Q


class SearchView(TemplateView):
    template_name = "search/search.html"


class MakeSearchView(TemplateView):
    template_name = "search/search_result.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        q = self.request.GET.get('q')
        sr = Product.objects.filter(
                                    (
                                        Q(
                                            Q(publisher__publisher__icontains = q) | 
                                            Q(title__icontains = q) | Q(year__icontains = q) 
                                            | Q(author__author__icontains = q)
                                            & Q(order_possibility = True)
                                        )
                                    )
                                    # 
                                    )
        # if order_possibility:
        #    sr = sr.filter(order_possibility == True)
        context['results'] = sr
        return context
