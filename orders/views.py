from django.views.generic import (
    UpdateView, DetailView, ListView,
    DeleteView, CreateView)
from django.urls import reverse_lazy

from .forms import CheckoutOrderForm
from .models import Order, OrderStatus
from django.shortcuts import get_object_or_404


class OrderCheckoutView(CreateView):
    model = Order
    template_name = 'cart/list-cart.html'
    form_class = CheckoutOrderForm

    def get_success_url(self):
        del self.request.session['cart_id']
        return reverse_lazy('orders:success', kwargs={'pk': self.object.pk})


class OrderCheckoutSuccess(DetailView):
    model = Order
    template_name = 'orders/success.html'


class OrdersListAdmin(ListView):
    model = Order
    template_name = 'orders/list-admin.html'
    paginate_by = 1

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["active"] = "orders"
        context["total"] = self.object_list.count()
        return context


class OrderDelete(DeleteView):
    model = Order
    template_name = 'orders/delete-order.html'

    def get_success_url(self):
        return self.request.POST.get('next', '/orders/list-admin')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["next"] = self.request.GET.get('next', '/')
        return context


class OrderDetailView(DetailView):
    template_name = 'orders/detail-order.html'
    model = Order

    def get_context_data(self, **kwargs):
        context = super(OrderDetailView, self).get_context_data(**kwargs)
        Order.objects.filter(pk=self.kwargs.get('pk'), status="1").update(status="2")
        return context

    # способ 2 ч\з get_object
    # def get_object(self, *args, **kwargs):
    #     obj = super().get_object(*args, **kwargs)
    #     obj.status = OrderStatus.objects.get(pk=2)
    #     obj.save()
    #     return obj


class OrderUpdateView(UpdateView):
    model = Order
    template_name = 'orders/update-order.html'
    fields = ['cart',
              'status',
              'phone',
              'email',
              'delivery_address',
              'comments',
              ]

    def get_success_url(self):
        return self.request.POST.get('next', '/orders/list-admin')
