from rest_framework import serializers
from .models import Product
from reference.serializers import AuthorSerializer

class ProductSerializer(serializers.ModelSerializer):
    author = AuthorSerializer(many = True)
    class Meta:
        model = Product
        fields = (
            'title', 
            'product_photo',
            'price',
            'series',
            'author',
            'genre',
            'year',
            'pages',
            'cover',
            'size',
            'isbn_number',
            'weight',
            'age_restriction',
            'publisher',
            'manufacturer',
            'in_stock_value',
            'order_possibility',
            'rating',
            'addition_date',
            'last_change',
            'promotion',
            'voted',
            'category',
            )