from django import forms
from .models import *
from django.core.exceptions import ValidationError

class BookForm(forms.ModelForm):
    class Meta:
        model=Product
        fields=[
            'title',
            'product_photo',
            'price',
            'author',
            'series',
            'genre',
            'year',
            'pages',
            'cover',
            'size',
            'isbn_number',
            'weight',
            'age_restriction',
            'publisher',
            'manufacturer',
            'in_stock_value',
            'order_possibility',
            'addition_date',
            'last_change',
            'promotion',
            'category',
        ]
    # def clean(self):
    #     cleaned_data = super().clean()
    #     price = cleaned_data.get('price')
    #     if price != 5:
    #         self.add_error('price','only 5')
    #         raise ValidationError("must be 5!! from form")
    #     return cleaned_data