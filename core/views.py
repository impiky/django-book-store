from django.views.generic import TemplateView, FormView, DetailView, UpdateView
from django.conf import settings
from products.models import Product
from django.contrib.auth.models import User
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.views import PasswordChangeView, PasswordChangeDoneView
from .forms import ChangePasswordForm
from django.urls import reverse_lazy
from braces.views import PrefetchRelatedMixin

# Create your views here.
class UserHomeTemplateView(PrefetchRelatedMixin ,TemplateView):
    template_name = 'user-home.html'
    prefetch_related = ['products_set']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['last_books'] = Product.objects.all()
        context['logo'] = 'BookStore'

        return context

class UserHomeAboutTemplateView(TemplateView):
    template_name = 'about.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['logo'] = 'BookStore'
        return context

class UserHomeDeliveryTemplateView(TemplateView):
    template_name = 'delivery.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['logo'] = 'BookStore'
        return context

class ChangePasswordView(PasswordChangeView):
    template_name='password/change-password.html'
    form_class = ChangePasswordForm
    success_url = reverse_lazy('password_change_done')

class ChangePasswordDoneView(PasswordChangeDoneView):
    template_name='password/change-password-done.html'
    title = ('Пароль был изменен')

class UserHomeProfileTemplateView(UpdateView):
    template_name = 'profile.html'
    fields = [
                'first_name',
                'last_name',
                'email',
    ]
    model = User
    success_url = '/profile/'
    def get_object(self):
        user = self.request.user
        return user
  
