from django.test import TestCase
from .models import Author
from django.test import Client


class AuthorTestCase(TestCase):
    def setUp(self):
        Author.objects.create(author="Пушкин")
        Author.objects.create(author="Толстой")

    def test_author_str(self):
        """Animals that can speak are correctly identified"""
        pushkin = Author.objects.get(author="Пушкин")
        tolstoi = Author.objects.get(author="Толстой")
        for i in [(pushkin, 'Пушкин'), (tolstoi ,'Толстой')]:
            with self.subTest(i=i):
                self.assertEqual(str(i[0]), i[1])
    
    # def test_long_author_str(self):
    #     obj = Author.objects.create(author="Пушкин"*100)
    #     self.assertEqual(str(obj), "Пушкин")

class ClientTestCase(TestCase):
    def setUp(self):
        self.c = Client()

    def test_login_post_page(self):
        resp = self.c.post('/accounts/login/', {'user': 'admin', 'password': '123456'})
        print(resp.context)
        self.assertEqual(resp.status_code, 302)

