from django import forms
from .models import ProfileAppModel


class UserProfileAddressForm(forms.ModelForm):
    class Meta:
        model=ProfileAppModel
        fields=[
            'customer'
            'delivery_address',
        ]
        widgets = {'customer': HiddenInput()}