from django.db import models
from django.contrib.auth import get_user_model
from products.models import Product

User = get_user_model()


class Cart(models.Model):
    user = models.ForeignKey(User, related_name='Покупатель',
                             null=True, blank=True, on_delete=models.CASCADE)
    created_date = models.DateTimeField(
        verbose_name='Создано', auto_now=False, auto_now_add=True)
    updated_date = models.DateTimeField(
        verbose_name='Изменено', auto_now=True, auto_now_add=False)

    def __str__(self):
        return 'Корзина №{} для пользователя {}'.format(self.pk, self.user)
    
    @property
    def products_in_cart_count(self):
        total = 0
        for book in self.products.all():
            total += book.quantity
        return total

    @property
    def total_price(self):
        total = 0
        for book in self.products.all():
            total += book.price_total
        return total

    class Meta:
        verbose_name = 'Корзина'
        verbose_name_plural = 'Корзины'


class ProductsInCart(models.Model):
    cart = models.ForeignKey(
        Cart, related_name='products', on_delete=models.CASCADE)
    book = models.ForeignKey(
        Product, on_delete=models.CASCADE)
    quantity = models.IntegerField('Количество', default=1)

    def __str__(self):
        return "Товар {name}, кол-во {quantity} в корзине".format(
            name=self.book.title,
            quantity=self.quantity)
    
    @property
    def price(self):
        return self.book.price

    @property
    def price_total(self):
        return self.book.price * self.quantity

    class Meta:
        verbose_name = 'Товар в корзине'
        verbose_name_plural = 'Товары в корзине'
        unique_together = (
            ('cart', 'book'), 
        )