from django.db import models
from django.urls import reverse_lazy

# Create your models here.


class Series(models.Model):
    genre = models.CharField(verbose_name="Серия", max_length=30)
    description = models.CharField(verbose_name="Описание", max_length=200)

    def __str__(self):
        return self.genre


class Publisher(models.Model):
    publisher = models.CharField(verbose_name="Издательство", max_length=200)

    def __str__(self):
        return self.publisher


class Author(models.Model):

    author = models.CharField(verbose_name="Автор", max_length=200, blank=True)
    
    
    def __str__(self):
        return self.author


class Genre(models.Model):
    genre = models.CharField(verbose_name="Жанр", max_length=50)

    def __str__(self):
        return self.genre


class Manufacturer(models.Model):
    manufacturer = models.CharField(
        verbose_name="Изготовитель", max_length=100)

    def __str__(self):
        return self.manufacturer

class Category(models.Model):
    category = models.CharField(
        verbose_name="Категория", max_length=100)

    def __str__(self):
        return self.category

class OrderStatus(models.Model):
    """
    Docstring
    """
    list_url = reverse_lazy('ref-order-status:list')

    name = models.CharField(
        "Статус",
        max_length=200)
    description = models.CharField(
        "Описание",
        blank=True,
        null=True,
        max_length=200)


    def __str__(self):
        return self.name

    def get_view_url(self):
        return reverse_lazy('ref-order-status:detail', kwargs={'pk': self.pk})

    def get_update_url(self):
        return reverse_lazy('ref-order-status:update', kwargs={'pk': self.pk})

    def get_delete_url(self):
        return reverse_lazy('ref-order-status:delete', kwargs={'pk': self.pk})
    class Meta:
        verbose_name = "Статус заказа"
        verbose_name_plural = "Статусы заказа"