from django.contrib import admin
from . import models

class CartAdmin(admin.ModelAdmin):
    list_display = [
        'user',
        'created_date',
        'updated_date'
    ]

    class Meta:
        model = models.Cart

class ProductsInCartAdmin(admin.ModelAdmin):
    list_display = [
        'cart',
        'book',
        'quantity'
    ]

    class Meta:
        model = models.Cart

admin.site.register(models.Cart, CartAdmin)
admin.site.register(models.ProductsInCart, ProductsInCartAdmin)