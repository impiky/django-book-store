import json
import csv
from django.http import HttpResponse
from rest_framework.renderers import JSONRenderer
from rest_framework.views import APIView
from rest_framework.generics import RetrieveAPIView
from rest_framework import authentication, permissions
from .models import Author, Series
from .serializers import AuthorSerializer, SerieSerializer
from rest_framework import viewsets
from rest_framework.decorators import action
# from docxtpl import DocxTemplate
from io import BytesIO, StringIO


class ListAuthor(RetrieveAPIView):

    queryset = Author.objects.all()
    model = Author
    serializer_class = AuthorSerializer


class AuthorViewSet(viewsets.ModelViewSet):
    serializer_class = AuthorSerializer
    queryset = Author.objects.all()


class SerieViewSet(viewsets.ModelViewSet):
    serializer_class = SerieSerializer
    queryset = Series.objects.all()

    @action(detail=False, methods=['get'])
    def getcsv(self, request):
        serializer = self.get_serializer(self.queryset, many=True)
        data = json.loads(JSONRenderer().render(serializer.data))
        response = HttpResponse(content_type='text/csv')
        response.charset = 'utf-8'
        response['Content-Disposition'] = 'attachment;filename=series.csv'
        header = ['id', 'Серия', 'Описание']
        writer = csv.writer(response, delimiter=';')
        writer.writerow(header)
        for key in data:
            writer.writerow([key['pk'], key['genre'], key['description']])
        content = response.content
        uncontent = content.decode('utf-8')
        asciicontent = uncontent.encode('cp1251', 'ignore')
        response.content
        return response

    @action(detail=False, methods=['get'])
    def getdocx(self, request):
        context = {}
        template_path = 'D:\\itacademy\\Homework\\Django\\src\\reference\\template.html'
        doc = DocxTemplate(template_path)
        doc.render(context)
        temp_file = BytesIO()
        doc.save(temp_file)
        response = HttpResponse(temp_file.getvalue(
        ), content_type='application/vnd.openxmlformats-officedocument.wordprocessingml.document')
        filename = 'parsed.docx'
        content = 'attachment; filename = {}'.format(filename)
        response['Content-Disposition'] = content
        return response
